package learnthreads;

public class SynchronizedCounter implements Runnable{

    private int total;

    public SynchronizedCounter() {
        this.total = 0;
    }

    public synchronized void increment() {
        this.total++;
    }

    public synchronized int getTotal() {
        return this.total;
    }

    public void run() {
        System.out.println("The current total is " + total);
        for (int i = 1; i <= 1000; i++) {
            increment();
            if (i % 25 == 0 ) {
                System.out.println("\n total is: " + getTotal() + "\n");
            }
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                System.out.println("Something went wrong.");
                System.exit(1);
            }
        }
        System.out.println("Finished");
    }
}
