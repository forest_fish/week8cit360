package learnthreads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Counters {
    public static void main(String[] args) {
        ExecutorService theService = Executors.newFixedThreadPool(2);

        SynchronizedCounter sc1 = new SynchronizedCounter();
        //SynchronizedCounter sc2 = new SynchronizedCounter();

        theService.execute(sc1);
        theService.execute(sc1);

        theService.shutdown();
    }
}
